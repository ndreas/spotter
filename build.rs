use std::path::Path;
use std::process::Command;

fn main() {
    if !cfg!(debug_assertions) {
        Command::new("npm")
            .args(&["run", "build"])
            .env("NODE_ENV", "production")
            .current_dir(&Path::new("./frontend"))
            .status()
            .unwrap();
    }
}
