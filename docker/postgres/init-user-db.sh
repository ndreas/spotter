#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
  create user spotter_dev with password 'spotter_dev';
  alter user spotter_dev superuser;
  create database spotter_dev;

  create user spotter_test with password 'spotter_test';
  alter user spotter_test superuser;
  create database spotter_test;
EOSQL
