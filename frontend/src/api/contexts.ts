import axios from 'axios';

export interface Context {
    id: string;
    track: Track;
}

export interface Track {
    id: number;
    title: string;
    artist: string;
    album: string;
    image?: string;
}

export async function listContexts(limit?: number, offset?: number): Promise<Context[]> {
    const params: any = {};

    if (limit) {
        params.limit = limit;
    }
    if (offset) {
        params.offset = offset;
    }

    const res = await axios.get('/contexts', {
        params,
    });
    return res.data!;
}
