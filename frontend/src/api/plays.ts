import axios from 'axios';

export interface Play {
    id: number;
    title: string;
    artist: string;
    album: string;
    image?: string;
    context: string;
    device_id: string;
    device: string;
}

export async function listPlays(limit?: number, offset?: number): Promise<Play[]> {
    const params: any = {};

    if (limit) {
        params.limit = limit;
    }
    if (offset) {
        params.offset = offset;
    }

    const res = await axios.get('/plays', {
        params,
    });
    return res.data!;
}
