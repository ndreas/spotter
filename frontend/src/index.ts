import Vue from 'vue';
import App from './App.vue';

const app = new Vue({
  render: (h) => h(App),
  el: '#app',
  components: {
    App
  }
})
