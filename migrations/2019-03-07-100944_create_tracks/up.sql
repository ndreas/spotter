create table tracks (
    id bigserial primary key not null

    , created_at timestamp with time zone not null default now()
    , updated_at timestamp with time zone not null default now()

    , title text not null
    , album text not null
    , artist text not null

    , context text not null

    , device_id text not null
    , device_name text not null
);

select diesel_manage_updated_at('tracks');
