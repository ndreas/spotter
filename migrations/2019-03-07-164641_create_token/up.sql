create table token (
    id smallint primary key default 0

    , access text not null
    , refresh text not null
);
