alter table tracks
    add column device_name text;

update tracks set device_name = raw_msg -> 'device' ->> 'name';

alter table tracks
    alter column device_name set not null;

alter table tracks
    drop constraint tracks_device_id_fk;

drop table devices;
