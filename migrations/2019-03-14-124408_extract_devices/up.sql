create table devices (
    id text primary key

    , created_at timestamp with time zone not null default now()
    , updated_at timestamp with time zone not null default now()

    , name text not null
    , type text
);

select diesel_manage_updated_at('devices');

insert into devices (id, name, type) 
    select device_id, device_name, raw_msg -> 'device' ->> 'type' as device_type
    from tracks
    order by updated_at desc
on conflict (id) do nothing;

alter table tracks
    add constraint tracks_device_id_fk
        foreign key (device_id)
        references devices (id);

alter table tracks
    drop column device_name;
