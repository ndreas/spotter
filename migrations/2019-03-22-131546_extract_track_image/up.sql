alter table tracks
    add column image text;

update tracks set image = coalesce(
    raw_msg #>> '{item,album,images,2,url}',
    raw_msg #>> '{item,album,images,1,url}',
    raw_msg #>> '{item,album,images,1,url}'
);
