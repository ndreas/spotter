create or replace view contexts as 
    select distinct on (context)
        context, title, album, artist, image, created_at as updated_at
    from tracks
    order by context asc, id desc;
