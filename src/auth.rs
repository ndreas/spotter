use std::time::Duration;

use actix::prelude::*;
use futures::Future;
use log::*;
use oauth2;

use crate::error::Result;

type TokenResult = Result<oauth2::Token>;

pub struct Auth {
    auth: oauth2::Config,
    subscribers: Vec<Recipient<NewToken>>,
    token_store: Option<Recipient<GetRefreshToken>>,
    refresh_mod: u64,
}

impl Auth {
    pub fn new(auth: oauth2::Config, refresh_mod: u64) -> Self {
        Auth {
            auth,
            subscribers: vec![],
            token_store: None,
            refresh_mod,
        }
    }

    pub fn with_subscriber(mut self, subscriber: Recipient<NewToken>) -> Self {
        self.subscribers.push(subscriber);
        self
    }

    pub fn with_token_store(mut self, token_store: Recipient<GetRefreshToken>) -> Self {
        self.token_store = Some(token_store);
        self
    }

    fn new_token(
        &mut self,
        prev_refresh_token: Option<String>,
        token: oauth2::Token,
        ctx: &mut <Self as Actor>::Context,
    ) -> TokenResult {
        info!("Received new token");
        debug!("{:#?}", token);

        for subscriber in self.subscribers.iter() {
            subscriber.do_send(NewToken {
                access: token.access_token.clone(),
                refresh: token.refresh_token.clone(),
            })?;
        }

        let refresh_token = token
            .refresh_token
            .as_ref()
            .or_else(|| prev_refresh_token.as_ref());

        if let (Some(exp), Some(refresh)) = (token.expires_in, refresh_token) {
            let dur = u64::from(exp) - self.refresh_mod;
            info!("Refreshing token in {} seconds", dur);
            ctx.notify_later(RefreshToken(refresh.to_owned()), Duration::from_secs(dur));
        }

        Ok(token)
    }
}

impl actix::Actor for Auth {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        if let Some(token_store) = self.token_store.as_ref() {
            token_store
                .send(GetRefreshToken)
                .map_err(|e| error!("Refresh error: {}", e))
                .into_actor(self)
                .map(|result, _act, ctx| {
                    if let Some(token) = result {
                        info!("Found stored refresh token");
                        debug!("{}", token);
                        ctx.notify(RefreshToken(token));
                    };
                })
                .wait(ctx);
        }
    }
}

pub struct NewToken {
    pub access: String,
    pub refresh: Option<String>,
}

impl Message for NewToken {
    type Result = ();
}

pub struct GetRefreshToken;

impl Message for GetRefreshToken {
    type Result = Option<String>;
}

pub struct GetAuthorizeURL;

impl Message for GetAuthorizeURL {
    type Result = String;
}

impl Handler<GetAuthorizeURL> for Auth {
    type Result = String;

    fn handle(&mut self, _msg: GetAuthorizeURL, _ctx: &mut Self::Context) -> Self::Result {
        self.auth.authorize_url().into_string()
    }
}

pub struct ExchangeCode(pub String);

impl Message for ExchangeCode {
    type Result = TokenResult;
}

impl Handler<ExchangeCode> for Auth {
    type Result = TokenResult;

    fn handle(&mut self, msg: ExchangeCode, ctx: &mut Self::Context) -> Self::Result {
        info!("Exchanging code with Spotify");
        debug!("{}", msg.0);
        let token = self.auth.exchange_code(msg.0)?;
        self.new_token(None, token, ctx)
    }
}

pub struct RefreshToken(pub String);

impl Message for RefreshToken {
    type Result = TokenResult;
}

impl Handler<RefreshToken> for Auth {
    type Result = TokenResult;

    fn handle(&mut self, msg: RefreshToken, ctx: &mut Self::Context) -> Self::Result {
        info!("Refreshing token");
        debug!("{}", msg.0);
        let token = self.auth.exchange_refresh_token(msg.0.clone())?;
        self.new_token(Some(msg.0), token, ctx)
    }
}
