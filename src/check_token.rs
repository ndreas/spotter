use actix_web::middleware::{Middleware, Started};
use actix_web::{http, HttpRequest, HttpResponse, Result};
use futures::Future;
use log::*;

use crate::spotify;
use crate::AppState;

pub struct CheckToken;

impl Middleware<AppState> for CheckToken {
    fn start(&self, req: &HttpRequest<AppState>) -> Result<Started> {
        if req.path().starts_with("/auth") {
            return Ok(Started::Done);
        }

        let fut = req
            .state()
            .spotify
            .send(spotify::HasToken)
            .from_err()
            .and_then(|has_token| {
                if has_token {
                    return Ok(None);
                }

                info!("Missing token, redirecting to /auth");
                Ok(Some(
                    HttpResponse::Found()
                        .header(http::header::LOCATION, "/auth")
                        .finish(),
                ))
            });

        Ok(Started::Future(Box::new(fut)))
    }
}
