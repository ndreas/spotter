use std::path::{Path, PathBuf};

use serde_derive::*;

#[derive(Debug, Deserialize)]
pub struct Config {
    pub base_url: String,
    pub poll_interval: u8,
    pub refresh_mod: u64,
    pub log: LogConfig,
    pub http: HttpConfig,
    pub db: DbConfig,
    pub spotify: SpotifyConfig,
}

#[derive(Debug, Deserialize)]
pub struct LogConfig {
    pub level: String,
    pub file: Option<PathBuf>,
}

#[derive(Debug, Deserialize)]
pub struct HttpConfig {
    pub listen: String,
}

#[derive(Debug, Deserialize)]
pub struct DbConfig {
    pub url: String,
}

#[derive(Debug, Deserialize)]
pub struct SpotifyConfig {
    pub client_id: String,
    pub client_secret: String,
}

impl Config {
    pub fn new() -> Result<Self, config::ConfigError> {
        let mut cfg = config::Config::new();

        cfg.set_default("base_url", "http://127.0.0.1:3000")?;
        cfg.set_default("poll_interval", 20)?;
        cfg.set_default("refresh_mod", 60)?;
        cfg.set_default("log.level", "warn")?;
        cfg.set_default("log.file", None::<Option<String>>)?;
        cfg.set_default("http.listen", "127.0.0.1:3000")?;

        if let Some(f) = [
            "spotter.toml",
            "/usr/local/etc/spotter.toml",
            "/etc/spotter.toml",
        ]
        .iter()
        .find(|p| Path::new(p).exists())
        {
            cfg.merge(config::File::with_name(f))?;
        }

        cfg.merge(config::Environment::with_prefix("SPOTTER_"))?;

        cfg.try_into()
    }
}
