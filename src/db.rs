use actix::prelude::*;
use diesel::prelude::*;
use log::*;

use crate::auth;
use crate::error::Result;

use crate::models::context::Context;
use crate::models::device::*;
use crate::models::play::*;
use crate::models::token::*;
use crate::models::track::*;

pub struct Db {
    conn: PgConnection,
}

impl Db {
    pub fn new(url: &str) -> Result<Self> {
        let conn = PgConnection::establish(url)?;
        info!("Connected to database");
        debug!("{}", url);
        Ok(Db { conn })
    }
}

impl Actor for Db {
    type Context = SyncContext<Self>;
}

pub struct RecordState {
    pub device: NewDevice,
    pub track: NewTrack,
}

type RecordStateResult = Result<bool>;

impl Message for RecordState {
    type Result = RecordStateResult;
}

impl Handler<RecordState> for Db {
    type Result = RecordStateResult;

    fn handle(&mut self, msg: RecordState, _ctx: &mut Self::Context) -> Self::Result {
        info!("Upserting device");
        debug!("{:#?}", msg.device);
        msg.device.upsert(&self.conn)?;

        info!("Recording latest track");
        debug!("{:#?}", msg.track);
        let recorded = msg.track.record_latest(&self.conn)?;
        Ok(recorded)
    }
}

impl Handler<auth::NewToken> for Db {
    type Result = ();
    fn handle(&mut self, msg: auth::NewToken, _ctx: &mut Self::Context) -> Self::Result {
        if let Some(refr) = msg.refresh {
            info!("New token");
            debug!("{:#?}", refr);
            match Token::new(msg.access, refr).upsert(&self.conn) {
                Ok(_) => info!("Token saved"),
                Err(e) => error!("Failed to save token: {}", e),
            };
        }
    }
}

impl Handler<auth::GetRefreshToken> for Db {
    type Result = Option<String>;
    fn handle(&mut self, _msg: auth::GetRefreshToken, _ctx: &mut Self::Context) -> Self::Result {
        use crate::schema::token::*;
        table.select(refresh).first(&self.conn).ok()
    }
}

pub struct ListPlays {
    pub limit: i64,
    pub offset: i64,
}

type ListPlaysResult = Result<Vec<Play>>;

impl Message for ListPlays {
    type Result = ListPlaysResult;
}

impl Handler<ListPlays> for Db {
    type Result = ListPlaysResult;

    fn handle(&mut self, msg: ListPlays, _ctx: &mut Self::Context) -> Self::Result {
        info!("Listing tracks");
        let res = Play::list(&self.conn, msg.limit, msg.offset)?;
        debug!("{:#?}", res);
        Ok(res)
    }
}

pub struct ListContexts {
    pub limit: i64,
    pub offset: i64,
}

type ListContextsResult = Result<Vec<Context>>;

impl Message for ListContexts {
    type Result = ListContextsResult;
}

impl Handler<ListContexts> for Db {
    type Result = ListContextsResult;

    fn handle(&mut self, msg: ListContexts, _ctx: &mut Self::Context) -> Self::Result {
        info!("Listing contexts");
        let res = Context::list(&self.conn, msg.limit, msg.offset)?;
        debug!("{:#?}", res);
        Ok(res)
    }
}
