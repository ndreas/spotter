use std::{error, fmt};

use log::*;

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    Actor(String),
    Auth(oauth2::TokenError),
    Database(diesel::result::Error),
    DatabaseConnection(diesel::ConnectionError),
    Reqwest(reqwest::Error),
    MissingToken,
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Error::Actor(ref err) => write!(f, "Actor error: {}", err),
            Error::Auth(ref err) => write!(f, "Auth error: {}", err),
            Error::Database(ref err) => write!(f, "Database error: {}", err),
            Error::DatabaseConnection(ref err) => write!(f, "Database connection error: {}", err),
            Error::Reqwest(ref err) => write!(f, "Reqwest error: {}", err),
            Error::MissingToken => write!(f, "{}", MISSING_TOKEN_DESCRIPTION),
        }
    }
}

static MISSING_TOKEN_DESCRIPTION: &str = "Missing token";

impl error::Error for Error {
    fn description(&self) -> &str {
        match *self {
            Error::Actor(ref err) => err,
            Error::Auth(ref err) => err.description(),
            Error::Database(ref err) => err.description(),
            Error::DatabaseConnection(ref err) => err.description(),
            Error::Reqwest(ref err) => err.description(),
            Error::MissingToken => MISSING_TOKEN_DESCRIPTION,
        }
    }

    fn cause(&self) -> Option<&error::Error> {
        match *self {
            Error::Auth(ref err) => Some(err),
            Error::Database(ref err) => Some(err),
            Error::DatabaseConnection(ref err) => Some(err),
            Error::Reqwest(ref err) => Some(err),
            _ => None,
        }
    }
}

impl From<actix::MailboxError> for Error {
    fn from(err: actix::MailboxError) -> Self {
        warn!("Mailbox error: {}", err);
        Error::Actor(format!("{}", err))
    }
}

impl<T> From<actix::prelude::SendError<T>> for Error {
    fn from(err: actix::prelude::SendError<T>) -> Self {
        warn!("Actor error: {}", err);
        Error::Actor(format!("{}", err))
    }
}

impl From<diesel::ConnectionError> for Error {
    fn from(err: diesel::ConnectionError) -> Self {
        warn!("Database connection error: {}", err);
        Error::DatabaseConnection(err)
    }
}

impl From<diesel::result::Error> for Error {
    fn from(err: diesel::result::Error) -> Self {
        warn!("Database result error: {}", err);
        Error::Database(err)
    }
}

impl From<oauth2::TokenError> for Error {
    fn from(err: oauth2::TokenError) -> Self {
        warn!("Auth error: {}", err);
        Error::Auth(err)
    }
}

impl From<reqwest::Error> for Error {
    fn from(err: reqwest::Error) -> Self {
        warn!("Reqwest error: {}", err);
        Error::Reqwest(err)
    }
}
