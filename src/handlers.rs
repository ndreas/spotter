use std::borrow::Cow;

use actix::prelude::*;
use actix_web::{Body, HttpRequest, HttpResponse, Responder, State};

use crate::{auth, db, spotify};

mod auth_handlers;
pub use auth_handlers::*;

mod context_handlers;
pub use context_handlers::*;

mod play_handlers;
pub use play_handlers::*;

pub struct AppState {
    pub auth: Addr<auth::Auth>,
    pub db: Addr<db::Db>,
    pub spotify: Addr<spotify::Spotify>,
}

#[derive(RustEmbed)]
#[folder = "frontend/dist/"]
struct Asset;

fn asset_to_body(content: Cow<'static, [u8]>) -> Body {
    match content {
        Cow::Borrowed(b) => b.into(),
        Cow::Owned(b) => b.into(),
    }
}

pub fn index_handler(_: State<AppState>) -> impl Responder {
    let asset = Asset::get("index.html")?;
    Some(HttpResponse::Ok().body(asset_to_body(asset)))
}

pub fn asset_handler(req: HttpRequest<AppState>) -> impl Responder {
    match Asset::get(&req.path()[1..]) {
        Some(content) => HttpResponse::Ok().body(asset_to_body(content)),
        None => HttpResponse::NotFound().body(""),
    }
}
