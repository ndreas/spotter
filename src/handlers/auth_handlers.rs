use actix_web::{http, AsyncResponder, FutureResponse, HttpResponse, Query, State};
use futures::Future;
use log::*;
use serde_derive::*;

use super::AppState;
use crate::auth;

pub fn auth_handler(state: State<AppState>) -> FutureResponse<HttpResponse> {
    state
        .auth
        .send(auth::GetAuthorizeURL)
        .from_err()
        .map(|url| {
            info!("Redirecting to {}", url);
            HttpResponse::Found()
                .header(http::header::LOCATION, url)
                .finish()
        })
        .responder()
}

#[derive(Deserialize)]
pub struct AuthCallbackParams {
    code: String,
}

pub fn auth_callback_handler(
    state: State<AppState>,
    params: Query<AuthCallbackParams>,
) -> FutureResponse<HttpResponse> {
    state
        .auth
        .send(auth::ExchangeCode(params.code.to_owned()))
        .from_err()
        .and_then(|_| {
            Ok(HttpResponse::Found()
                .header(http::header::LOCATION, "/")
                .finish())
        })
        .responder()
}
