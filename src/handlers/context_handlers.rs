use actix_web::{error, AsyncResponder, FutureResponse, HttpResponse, Query, State};
use futures::Future;
use log::*;
use serde_derive::Deserialize;

use super::AppState;
use crate::db;

#[derive(Deserialize)]
pub struct ListContextsParams {
    pub limit: Option<i64>,
    pub offset: Option<i64>,
}

pub fn list_contexts_handler(
    state: State<AppState>,
    params: Query<ListContextsParams>,
) -> FutureResponse<HttpResponse> {
    state
        .db
        .send(db::ListContexts {
            limit: params.limit.unwrap_or(10),
            offset: params.offset.unwrap_or(0),
        })
        .flatten()
        .map_err(|e| {
            error!("Failed to list contexts: {}", e);
            error::ErrorInternalServerError("Failed to list plays")
        })
        .from_err()
        .map(|plays| HttpResponse::Ok().json(plays))
        .responder()
}
