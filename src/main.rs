#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;
#[macro_use]
extern crate rust_embed;

mod auth;
mod check_token;
mod config;
mod db;
mod error;
mod handlers;
mod migration;
mod models;
mod schema;
mod spotify;
mod views;
mod worker;

use actix::prelude::*;
use actix_web::middleware;
use actix_web::{http, server, App};
use log::*;
use oauth2::Config as OAuth2;

use handlers::*;

fn main() -> Result<(), Box<std::error::Error>> {
    let cfg = config::Config::new()?;
    init_log(&cfg.log.level, cfg.log.file)?;
    info!("Spotify logger");

    migration::run(&cfg.db.url)?;

    let sys = actix::System::new("spotter");

    let mut auth_client = OAuth2::new(
        cfg.spotify.client_id,
        cfg.spotify.client_secret,
        "https://accounts.spotify.com/authorize",
        "https://accounts.spotify.com/api/token",
    );

    auth_client = auth_client.add_scope("user-read-playback-state");
    auth_client = auth_client.set_redirect_url(format!("{}/auth/callback", cfg.base_url));

    let spotify_addr = spotify::Spotify::new().start();

    let db_url = cfg.db.url.clone();
    let db_addr = SyncArbiter::start(2, move || match db::Db::new(&db_url) {
        Ok(db) => db,
        Err(e) => {
            error!("Failed to connect to database: {}", e);
            panic!();
        }
    });

    let auth_addr = auth::Auth::new(auth_client, cfg.refresh_mod)
        .with_subscriber(spotify_addr.clone().recipient())
        .with_subscriber(db_addr.clone().recipient())
        .with_token_store(db_addr.clone().recipient())
        .start();

    worker::Worker::new(cfg.poll_interval, spotify_addr.clone(), db_addr.clone()).start();

    server::new(move || {
        let state = AppState {
            auth: auth_addr.clone(),
            db: db_addr.clone(),
            spotify: spotify_addr.clone(),
        };
        App::with_state(state)
            .middleware(middleware::Logger::default())
            .middleware(check_token::CheckToken)
            .resource("/", |r| r.method(http::Method::GET).with(index_handler))
            .resource("/contexts", |r| {
                r.method(http::Method::GET).with(list_contexts_handler)
            })
            .resource("/plays", |r| {
                r.method(http::Method::GET).with(list_plays_handler)
            })
            .resource("/auth", |r| r.method(http::Method::GET).with(auth_handler))
            .resource("/auth/callback", |r| {
                r.method(http::Method::GET).with(auth_callback_handler)
            })
            .route("/{_:.*}", http::Method::GET, asset_handler)
    })
    .bind(&cfg.http.listen)?
    .start();

    info!("Running HTTP server on {}", cfg.http.listen);
    sys.run();
    Ok(())
}

fn init_log(
    level: &str,
    logfile: Option<std::path::PathBuf>,
) -> Result<(), Box<std::error::Error>> {
    let mut logger = fern::Dispatch::new();

    logger = logger.level(log::LevelFilter::Warn);

    logger = logger.level_for(
        "spotter",
        match level {
            "error" => log::LevelFilter::Error,
            "info" => log::LevelFilter::Info,
            "debug" => log::LevelFilter::Debug,
            "trace" => log::LevelFilter::Trace,
            _ => log::LevelFilter::Warn,
        },
    );

    logger = if let Some(path) = logfile {
        let file = reopen::Reopen::new(Box::new(move || {
            std::fs::OpenOptions::new()
                .create(true)
                .write(true)
                .append(true)
                .open(path.clone())
        }))?;

        file.handle().register_signal(libc::SIGHUP)?;

        let boxed_file: Box<dyn std::io::Write + std::marker::Send + 'static> = Box::new(file);

        logger
            .format(|out, message, record| {
                out.finish(format_args!(
                    "{} [{}] [{}] {}",
                    chrono::Local::now().format("%+"),
                    record.level(),
                    record.target(),
                    message
                ))
            })
            .chain(boxed_file)
    } else {
        use fern::colors::{Color, ColoredLevelConfig};

        let color = ColoredLevelConfig::new()
            .error(Color::Red)
            .warn(Color::Yellow)
            .info(Color::Green)
            .debug(Color::White)
            .trace(Color::BrightBlack);

        logger
            .format(move |out, message, record| {
                out.finish(format_args!(
                    "{} [{}] [{}] {}",
                    chrono::Local::now().format("%+"),
                    color.color(record.level()),
                    record.target(),
                    message
                ))
            })
            .chain(std::io::stderr())
    };

    logger.apply()?;

    Ok(())
}
