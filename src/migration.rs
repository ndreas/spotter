use diesel::prelude::*;
use log::*;

embed_migrations!();

#[cfg(not(debug))]
pub fn run(url: &str) -> Result<(), Box<std::error::Error>> {
    info!("Running migrations");
    debug!("{}", url);
    let conn = PgConnection::establish(&url)?;
    embedded_migrations::run(&conn)?;

    Ok(())
}

#[cfg(debug)]
pub fn run(url: &str) -> Result<(), Box<std::error::Error>> {
    Ok(())
}
