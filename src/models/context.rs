use diesel::prelude::*;
use serde_derive::Serialize;

use crate::error::Result;

#[derive(Debug, Queryable, Serialize)]
pub struct Context {
    pub id: String,
    pub track: Track,
}
#[derive(Debug, Queryable, Serialize)]
pub struct Track {
    pub title: String,
    pub album: String,
    pub artist: String,
    pub image: Option<String>,
}

impl Context {
    pub fn list(conn: &PgConnection, limit: i64, offset: i64) -> Result<Vec<Context>> {
        use crate::views::contexts::dsl::*;

        Ok(contexts
            .select((context, (title, album, artist, image)))
            .order(updated_at.desc())
            .limit(limit)
            .offset(offset)
            .load(conn)?)
    }
}
