use chrono::{DateTime, Utc};
use diesel::prelude::*;

use crate::schema::devices;

#[derive(Debug, Queryable, Identifiable)]
pub struct Device {
    pub id: String,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
    pub name: String,
    pub type_: String,
}

#[derive(Debug, Insertable, AsChangeset)]
#[table_name = "devices"]
pub struct NewDevice {
    pub id: String,
    pub name: String,
    pub type_: String,
}

impl NewDevice {
    pub fn upsert(&self, conn: &PgConnection) -> QueryResult<usize> {
        use crate::schema::devices::dsl::*;

        self.insert_into(devices)
            .on_conflict(id)
            .do_update()
            .set(self)
            .execute(conn)
    }
}
