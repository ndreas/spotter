use diesel::prelude::*;
use serde_derive::Serialize;

use crate::error::Result;

#[derive(Debug, Queryable, Serialize)]
pub struct Play {
    pub id: i64,
    pub title: String,
    pub artist: String,
    pub album: String,
    pub image: Option<String>,
    pub context: String,
    pub device_id: String,
    pub device: String,
}

impl Play {
    pub fn list(conn: &PgConnection, limit: i64, offset: i64) -> Result<Vec<Play>> {
        use crate::schema::devices::dsl as devices;
        use crate::schema::tracks::dsl::*;

        Ok(tracks
            .inner_join(devices::devices)
            .select((
                id,
                title,
                artist,
                album,
                image,
                context,
                devices::id,
                devices::name,
            ))
            .order(id.desc())
            .limit(limit)
            .offset(offset)
            .load(conn)?)
    }
}
