use diesel::prelude::*;

use crate::schema::token;

#[derive(Debug, Insertable, AsChangeset)]
#[table_name = "token"]
pub struct Token {
    id: i16,
    access: String,
    refresh: String,
}

impl Token {
    pub fn new(access: String, refresh: String) -> Token {
        Token {
            id: 0,
            access,
            refresh,
        }
    }

    pub fn upsert(&self, conn: &PgConnection) -> QueryResult<usize> {
        use crate::schema::token::dsl::*;

        self.insert_into(token)
            .on_conflict(id)
            .do_update()
            .set(self)
            .execute(conn)
    }
}
