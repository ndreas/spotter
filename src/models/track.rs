use chrono::{DateTime, Utc};
use diesel::prelude::*;

use crate::schema::tracks;

#[derive(Debug, Queryable, Identifiable)]
pub struct Track {
    pub id: i64,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
    pub title: String,
    pub album: String,
    pub artist: String,
    pub context: String,
    pub device_id: String,
    pub raw_msg: serde_json::Value,
    pub image: Option<String>,
}

#[derive(Debug, Insertable)]
#[table_name = "tracks"]
pub struct NewTrack {
    pub title: String,
    pub album: String,
    pub artist: String,
    pub context: String,
    pub device_id: String,
    pub raw_msg: serde_json::Value,
    pub image: Option<String>,
}

impl NewTrack {
    pub fn record_latest(&self, conn: &PgConnection) -> QueryResult<bool> {
        use crate::schema::tracks::dsl::*;

        let latest: Option<Track> = tracks.order(id.desc()).first(conn).optional()?;

        if let Some(track) = latest.as_ref() {
            if self.album == track.album
                && self.artist == track.artist
                && self.context == track.context
                && self.device_id == track.device_id
                && self.title == track.title
            {
                return Ok(false);
            }
        }

        self.insert_into(tracks).execute(conn)?;

        Ok(true)
    }
}
