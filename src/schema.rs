table! {
    devices (id) {
        id -> Text,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
        name -> Text,
        #[sql_name = "type"]
        type_ -> Nullable<Text>,
    }
}

table! {
    token (id) {
        id -> Int2,
        access -> Text,
        refresh -> Text,
    }
}

table! {
    tracks (id) {
        id -> Int8,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
        title -> Text,
        album -> Text,
        artist -> Text,
        context -> Text,
        device_id -> Text,
        raw_msg -> Jsonb,
        image -> Nullable<Text>,
    }
}

joinable!(tracks -> devices (device_id));

allow_tables_to_appear_in_same_query!(
    devices,
    token,
    tracks,
);
