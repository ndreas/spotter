use actix::prelude::*;
use log::*;
use reqwest::Client;

use crate::auth;
use crate::error::Error;

mod currently_playing_context;
pub use currently_playing_context::*;

pub struct Spotify {
    token: Option<String>,
}

impl Spotify {
    pub fn new() -> Self {
        Spotify { token: None }
    }
}

impl Actor for Spotify {
    type Context = Context<Self>;
}

impl Handler<auth::NewToken> for Spotify {
    type Result = ();
    fn handle(&mut self, msg: auth::NewToken, _ctx: &mut Self::Context) -> Self::Result {
        info!("Received new token");
        debug!("{}", msg.access);
        self.token = Some(msg.access);
    }
}

pub struct HasToken;

impl Message for HasToken {
    type Result = bool;
}

impl Handler<HasToken> for Spotify {
    type Result = bool;
    fn handle(&mut self, _msg: HasToken, _ctx: &mut Self::Context) -> Self::Result {
        self.token.is_some()
    }
}

pub struct GetPlayerState;
type PlayerStateResult = Result<CurrentlyPlayingContext, Error>;

impl Message for GetPlayerState {
    type Result = PlayerStateResult;
}

impl Handler<GetPlayerState> for Spotify {
    type Result = PlayerStateResult;

    fn handle(&mut self, _msg: GetPlayerState, _ctx: &mut Self::Context) -> Self::Result {
        if self.token.is_none() {
            warn!("Missing token");
            return Err(Error::MissingToken);
        }

        let mut res = Client::new()
            .get("https://api.spotify.com/v1/me/player")
            .bearer_auth(self.token.as_ref().unwrap())
            .send()?
            .error_for_status()?;

        let data: CurrentlyPlayingContext = res.json()?;
        info!("Fetched player state");
        debug!("{:#?}", data);
        Ok(data)
    }
}
