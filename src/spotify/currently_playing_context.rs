use std::collections::HashMap;

use serde_derive::*;

// https://developer.spotify.com/documentation/web-api/reference/player/get-information-about-the-users-current-playback/

#[derive(Debug, Serialize, Deserialize)]
pub struct CurrentlyPlayingContext {
    pub device: Device,
    pub repeat_state: RepeatState,
    pub shuffle_state: bool,
    pub context: Option<PlayContext>,
    pub timestamp: u64,
    pub progress_ms: Option<u64>,
    pub is_playing: bool,
    pub item: Option<FullTrack>,
    pub currently_playing_type: CurrentlyPlayingType,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Device {
    pub id: String,
    pub is_active: bool,
    pub is_restricted: bool,
    pub name: String,
    #[serde(rename = "type")]
    pub device_type: String,
    pub volume_percent: u8,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum RepeatState {
    Off,
    Track,
    Context,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct PlayContext {
    #[serde(default)]
    pub external_urls: HashMap<String, String>,
    pub href: String,
    #[serde(rename = "type")]
    pub context_type: String,
    pub uri: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct FullTrack {
    pub album: SimplifiedAlbum,
    #[serde(default)]
    pub artists: Vec<SimplifiedArtist>,
    pub disc_number: u8,
    pub duration_ms: u64,
    #[serde(default)]
    pub external_urls: HashMap<String, String>,
    pub href: String,
    pub id: String,
    pub name: String,
    pub track_number: u16,
    pub uri: String,
    pub is_local: bool,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SimplifiedAlbum {
    pub album_type: AlbumType,
    #[serde(default)]
    pub artists: Vec<SimplifiedArtist>,
    #[serde(default)]
    pub external_urls: HashMap<String, String>,
    pub href: String,
    pub id: String,
    pub images: Vec<Image>,
    pub name: String,
    pub release_date: String,
    pub release_date_precision: ReleaseDatePrecision,
    pub uri: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct SimplifiedArtist {
    #[serde(default)]
    pub external_urls: HashMap<String, String>,
    pub href: String,
    pub id: String,
    pub name: String,
    pub uri: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum AlbumType {
    Album,
    Single,
    Compilation,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum CurrentlyPlayingType {
    Unknown,
    Track,
    Episode,
    Ad,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Image {
    pub url: String,
    pub height: Option<u32>,
    pub width: Option<u32>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum ReleaseDatePrecision {
    Year,
    Month,
    Day,
}
