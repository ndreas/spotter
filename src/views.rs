table! {
    contexts (context) {
        context -> Text,
        title -> Text,
        album -> Text,
        artist -> Text,
        image -> Nullable<Text>,
        updated_at -> Timestamptz,
    }
}
