use std::time::Duration;

use actix::prelude::*;
use futures::future::{self, Either};
use futures::Future;
use log::*;

use crate::db;
use crate::models::device::NewDevice;
use crate::models::track::NewTrack;
use crate::spotify;

pub struct Worker {
    interval: u8,
    spotify: Addr<spotify::Spotify>,
    db: Addr<db::Db>,
}

impl Worker {
    pub fn new(interval: u8, spotify: Addr<spotify::Spotify>, db: Addr<db::Db>) -> Self {
        Worker {
            interval,
            spotify,
            db,
        }
    }
}

impl Actor for Worker {
    type Context = Context<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        info!("Scheduling worker to run every {} seconds", self.interval);
        ctx.run_interval(Duration::from_secs(self.interval.into()), |_, ctx| {
            ctx.notify(RecordState);
        });
    }
}

pub struct RecordState;

impl Message for RecordState {
    type Result = ();
}

impl Handler<RecordState> for Worker {
    type Result = ();

    fn handle(&mut self, _msg: RecordState, ctx: &mut Self::Context) -> Self::Result {
        info!("Recording state");
        let db_addr = self.db.clone();

        self.spotify
            .send(spotify::GetPlayerState)
            .flatten()
            .and_then(move |state| match to_record_state(state) {
                Some(msg) => Either::A(db_addr.send(msg).flatten().map(|r| {
                    if r {
                        info!("State saved")
                    } else {
                        info!("Skipped saving state")
                    }
                })),
                None => Either::B(future::ok(()).map(|_| info!("No relevant data to record"))),
            })
            .map_err(|e| warn!("Spotify error: {}", e))
            .into_actor(self)
            .wait(ctx);
    }
}

fn to_record_state(play: spotify::CurrentlyPlayingContext) -> Option<db::RecordState> {
    let raw_msg = serde_json::value::to_value(&play).ok()?;

    if !play.is_playing {
        return None;
    }

    let track = play.item?;
    let artists: Vec<&str> = track.artists.iter().map(|a| a.name.as_str()).collect();
    let context = play.context?;
    let device_id = play.device.id;
    let image = track.album.images.last().map(|img| img.url.clone());

    Some(db::RecordState {
        device: NewDevice {
            id: device_id.clone(),
            name: play.device.name,
            type_: play.device.device_type,
        },
        track: NewTrack {
            title: track.name,
            album: track.album.name,
            artist: artists.join(", "),
            context: context.uri,
            device_id: device_id,
            image: image,
            raw_msg: raw_msg,
        },
    })
}
